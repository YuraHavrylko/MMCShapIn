﻿using System;
using Microsoft.Win32;

namespace Microsoft.ManagementConsole.Samples
{
    public class ModifyRegistry
    {
        public string Path { get; set; }

        public string GetValueKeyByName<T>(string registryKeyPath, string value, T defaultValue = default(T))
        {
            return Convert.ToString(Registry.GetValue(registryKeyPath, value, defaultValue));
        }

        public bool SetValueKeyByName(string KeyName, object Value)
        {
            try
            {
                Registry.SetValue(Path, KeyName, Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
    
        }

    }
}