﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;

namespace Microsoft.ManagementConsole.Samples
{

    public class SelectionListView : MmcListView
    {

        ModifyRegistry registry = new ModifyRegistry() { Path = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" };

        private string CommonProgramSnapIn = "NoCommonGroups";

        public SelectionListView()
        {
        }
      
        protected override void OnInitialize(AsyncStatus status)
        {
          
            base.OnInitialize(status);

            
            this.Columns[0].Title = "Command";
            this.Columns[0].SetWidth(300);

            
            this.Columns.Add(new MmcListViewColumn("Status", 200));

            
            this.Mode = MmcListViewMode.Report; 
            
            this.SelectionData.EnabledStandardVerbs = StandardVerbs.Refresh;

            
            Refresh();
        }
        
        protected override void OnSelectionChanged(SyncStatus status)
        {
            if (this.SelectedNodes.Count == 0)
            {
                this.SelectionData.Clear();
            }
            else
            {
                this.SelectionData.Update(GetSelectedCommand(), this.SelectedNodes.Count > 1, null, null);
                this.SelectionData.ActionsPaneItems.Clear();
                this.SelectionData.ActionsPaneItems.Add(new Action("Start", "Delete the group Default Programs in the Start menu", -1, "Start"));
                this.SelectionData.ActionsPaneItems.Add(new Action("Stop", "Add the group Default Programs in the Start menu", -1, "Stop"));
            }
        }

        protected override void OnRefresh(AsyncStatus status)
        {
            Refresh();
        }
        
        protected override void OnSelectionAction(Action action, AsyncStatus status)
        {
            switch ((string)action.Tag)
            {
                case "Start":
                    {
                        Start();
                        break;
                    }
                case "Stop":
                    {
                        Stop();
                        break;
                    }
            }
            Refresh();
        }

        private void Start()
        {
            if (!registry.SetValueKeyByName(CommonProgramSnapIn, 1))
            {
                throw new Exception("Error in set value");
            }
            
        }

        private void Stop()
        {
            if (!registry.SetValueKeyByName(CommonProgramSnapIn, 0))
            {
                throw new Exception("Error in set value");
            }
        }

        
        private string GetSelectedCommand()
        {
            StringBuilder selectedCommand = new StringBuilder();

            foreach (ResultNode resultNode in this.SelectedNodes)
            {

                selectedCommand.Append(resultNode.DisplayName + "\n");
            }

            return selectedCommand.ToString();
        }
        
        public void Refresh()
        {
            
            this.ResultNodes.Clear();

            string[][] snapinStrings = { new string[] { CommonProgramSnapIn, registry.GetValueKeyByName<int>(registry.Path, CommonProgramSnapIn, 1) }};

            foreach (string[] snapin in snapinStrings)
            {
                ResultNode node = new ResultNode();
                node.DisplayName = snapin[0];
                node.SubItemDisplayNames.Add(snapin[1]);

                this.ResultNodes.Add(node);
            }
        }
    } 
} 