﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using Microsoft.ManagementConsole;

[assembly: PermissionSetAttribute(SecurityAction.RequestMinimum, Unrestricted = true)]
namespace Microsoft.ManagementConsole.Samples
{
    [RunInstaller(true)]
    public class InstallUtilSupport : SnapInInstaller
    {
    }
   
    [SnapInSettings("{CE355EF6-9E3D-42c8-B725-95CCC761B9D9}",
       DisplayName = "Start/Stop some feature by current user",
        Description = "Shows mmc shap-in")]
    public class SelectionListviewSnapIn : SnapIn
    {
       
        public SelectionListviewSnapIn()
        {

            this.RootNode = new ScopeNode();
            this.RootNode.DisplayName = "Start/Stop some feature by current user";

            MmcListViewDescription lvd = new MmcListViewDescription();
            lvd.DisplayName = "Shows mmc shap-in";
            lvd.ViewType = typeof(SelectionListView);
            lvd.Options = MmcListViewOptions.ExcludeScopeNodes;


            this.RootNode.ViewDescriptions.Add(lvd);
            this.RootNode.ViewDescriptions.DefaultIndex = 0;
        }
    }

}